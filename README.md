# `team-david-and-daddy`

[![Education: ILTexas: Grade 4][ilt-shield]][wiki-home] David's ILTexas ([Grade 4 ![Go to ILTexas K-8 public site][octicon-link-external]][iltk8-grade-4]) [**schedule & class links**][edu-ilt-grade-4-schedule-classes], [teachers][edu-ilt-grade-4-teachers], and [student norms][edu-ilt-grade-4-norms].

---

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a> This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

[1]: .github/assets/img/icons8-mission-64.png
[2]: .github/assets/img/icons8-task-planning-64.png
[3]: https://gitlab.com/gregswindle/team-david-and-daddy/-/wikis/Education/ILTexas/Grade-4/Grade-4-Schedule-and-Classes
[iltk8]: https://www.iltexaskk8.com/
[iltk8-grade-4]: https://www.iltexaskk8.com/4th-grade "Open ILTexas Keller K-8 4th Grade webpage for teachers, news, lessons, supply list & resources..."
[ilt-shield]: https://static.wixstatic.com/media/e2487e_85936ee681c840b188f9c262654d0bf4~mv2.png/v1/fill/w_120,h_140,al_c,q_85,usm_0.66_1.00_0.01/Shield%20Full_Text%20Below.webp
[wiki-home]: https://gitlab.com/gregswindle/team-david-and-daddy/-/wikis/home
[edu-ilt-grade-4-schedule-classes]: https://gitlab.com/gregswindle/team-david-and-daddy/-/wikis/Education/ILTexas/Grade-4/Grade-4-Schedule-and-Classes "Homeroom teacher, class schedule & Zoom class links"
[edu-ilt-grade-4-teachers]: https://gitlab.com/gregswindle/team-david-and-daddy/-/wikis/Education/ILTexas/Grade-4/Teachers "Teachers Newsom, Vickers, Ollero, Alicea, Christian, Watkins, Rosario, Athletic coaches, and the Nurse"
[edu-ilt-grade-4-norms]: https://gitlab.com/gregswindle/team-david-and-daddy/-/wikis/Education/ILTexas/Student-Norms "How students are expected to behave"
[octicon-link-external]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/link-external.svg
